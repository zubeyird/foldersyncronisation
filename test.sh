!/bin/bash

function dirEqualize(){
  echo "In the function"
  local source="$1"
  local dest="$2"
  echo "$source" 
  echo "$dest"

  for file in "$source"/*; do
    if [[ -d "$file" ]]; then
      local rel_path=$(echo "${file#$source}" | sed 's/ /\\ /g')
      local unsyncFolders
      local file="$(echo "${file}" | sed 's/ /\\ /g')"
      echo "source:"
      echo "$file"
      echo "dest :"
      echo "$(dirname "$dest$rel_path")"
      if "${file}" "$dest$rel_path"; then
        # File does not exist in the destination folder, sync it
        cp -r -v "${file}" "$(dirname "$dest$rel_path")" 
      fi
      echo "$rel_path"

    fi
done
}
echo "Script execution completed successfully."
echo "Next steps:"
echo "1. Desired synchronization folder location should be specified."

read -p "Enter the number of folders: " folder_count

# Validate the folder count
if ! [[ $folder_count =~ ^[1-9][0-9]*$ ]]; then
  echo "Invalid folder count. Please enter a positive integer."
  exit 1
fi

# Array to store the folder destinations
folders=()

# Loop to prompt the user for folder destinations
for ((i=0; i<folder_count; i++)); do
  read -p "Enter folder destination $((i+1)): " folder
  folders+=("$folder")
done

echo ""
echo "You've entered destinations."

for ((i=0; i<${#folders[@]}-1; i++)); do
  dir1=$(basename "${folders[i]}")
  dir2=$(basename "${folders[i+1]}")
  size1=$(du -sh "${folders[i]}" | awk '{print $1}')
  size2=$(du -sh "${folders[i+1]}" | awk '{print $1}')

  if [[ "$dir1" == "$dir2" ]]; then
    echo "Folder $((i+1)) and Folder $((i+2)) have the same directory name: $dir1"
    break
  else
    echo "Folder $((i+1)) and Folder $((i+2)) have different directory names"
  fi

  if [[ "$size1" == "$size2" ]]; then
    echo "Folder $((i+1)) and Folder $((i+2)) have the same size: $size1"
  else
    echo "Folder $((i+1)) and Folder $((i+2)) have different sizes"
  fi


  echo "Directories have the same files:"
  echo "$dir1:"
  echo "$files1"
  echo "$size1"

  echo "$dir2:"
  echo "$files2"
  echo "$size2"

  dir1="${folders[i]}"
  dir2="${folders[i+1]}"

  dirEqualize "$dir1" "$dir2"
done

