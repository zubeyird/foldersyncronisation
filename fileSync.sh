!/bin/bash

# Project : fileSyncronisation
# File    : fileSyncronisation bash script
# Author  : Zubeyir Durgut
# Date    : 2023-07-10 22:36:38
# License : GPL-3
# Version : 1
# Contact : zubeyird@ktu.edu.tr
# Aim     : This code is a folder sycronisation script
# 
# ToDo    : 
# 	 	*** equal folder names parameter
# 	 	*** subdirectory step comparision
# 	 	*** measure foldersizes 
# 	 	*** file size comparision 
# 	 	    if sizes are equal skip
# 	 	    if sizes not equal 
# 	 	        comparision parameters for appending folder change
# 	 	            bigger sized file to small size
# 	 	            append file date and time should be older 
# Changes : 
# Test    :  [ ] Test for write protected files 
# Steps   : 
#
# Recursively compare folders: Use a recursive function to iterate through the folders and subfolders in both source and destination directories. 
# Compare the contents of each folder to determine any mismatches.

# Check for missing folders: If a folder exists in the source directory but not in the destination directory, create the corresponding folder in the destination directory.

# Compare file size and date: For each file in the source and destination folders, compare the file size and modification date. If the file does not exist in the destination folder, or if the size or date is different, perform the synchronization.

# Synchronize files: Copy or move the files from the source to the destination folder if they need to be synchronized. You can use the cp or mv command in your bash script to handle this.

# Handle exceptions: If a file exists in both the source and destination folders but has a smaller size or an older modification date in the source folder, you can handle it as an exception. You can log or display a message to acknowledge this exception and decide how you want to proceed.

# this is a commnt



# Functions

function dirEqualize(){
  echo "In the function"
  local source="$1"
  local dest="$2"
  echo "$source" 
  echo "$dest"

  for file in "$source"/*; do
    # Compare the contents of each folder to determine any mismatches by
    # size 
    # date
    #  diff 

    #  for ((i=0; i<folder_count; i++)); do
    #  if [${#fSource[@]} != ${#fDest[@]}];

    #    files1=$(ls "${source}")
    #    files2=$(ls "${dest}")
    #    size1=$(du -sh "${folders[i]}" | awk '{print $1}')
    #    size2=$(du -sh "${folders[i+1]}" | awk '{print $1}')
    #    read -p "Enter folder destination $((i+1)): " folder
    #    folders+=("$folder")
    #  done

    #  if 
    #  STDERR
    if [[ -d "$file" ]]; then
      local rel_path=$(echo "${file#$source}" | sed 's/ /\\ /g')
      local unsyncFolders
      local file="$(echo "${file}" | sed 's/ /\\ /g')"
      echo "source:"
      echo "$file"
      echo "dest :"
      echo "$(dirname "$dest$rel_path")"
      if "${file}" "$dest$rel_path"; then
        # File does not exist in the destination folder, sync it
        cp -r -v "${file}" "$(dirname "$dest$rel_path")" 
      fi
      echo "$rel_path"


    elif [[ -f "$file" ]]; then
      local rel_path="${file#$source}"
      local dest_file="$dest$rel_path"

      if [[ ! -f "$dest_file" ]]; then
        # File does not exist in the destination folder, sync it
        cp "$file" "$dest_file"
      else
        # Compare file size and modification date
        source_size=$(stat -c%s "$file")
        dest_size=$(stat -c%s "$dest_file")
        source_date=$(stat -c%Y "$file")
        dest_date=$(stat -c%Y "$dest_file")

        if [[ "$source_size" -ne "$dest_size" ]] || [[ "$source_date" -gt "$dest_date" ]]; then
          # File needs to be synchronized
          cp "$file" "$dest_file"
        elif [[ "$source_size" -lt "$dest_size" ]] || [[ "$source_date" -lt "$dest_date" ]]; then
          # Exception: Smaller size or older modification date in source
          echo "Exception: $file has a smaller size or older modification date in the source folder."
        fi
      fi
    fi
done
}
echo "Script execution completed successfully."
echo "Next steps:"
echo "1. Desired synchronization folder location should be specified."

read -p "Enter the number of folders: " folder_count

# Validate the folder count
if ! [[ $folder_count =~ ^[1-9][0-9]*$ ]]; then
  echo "Invalid folder count. Please enter a positive integer."
  exit 1
fi

# Array to store the folder destinations
folders=()

# Loop to prompt the user for folder destinations
for ((i=0; i<folder_count; i++)); do
  read -p "Enter folder destination $((i+1)): " folder
  folders+=("$folder")
done

echo ""
echo "You've entered destinations."

for ((i=0; i<${#folders[@]}-1; i++)); do
  dir1=$(basename "${folders[i]}")
  dir2=$(basename "${folders[i+1]}")
  size1=$(du -sh "${folders[i]}" | awk '{print $1}')
  size2=$(du -sh "${folders[i+1]}" | awk '{print $1}')

  if [[ "$dir1" == "$dir2" ]]; then
    echo "Folder $((i+1)) and Folder $((i+2)) have the same directory name: $dir1"
    break
  else
    echo "Folder $((i+1)) and Folder $((i+2)) have different directory names"
  fi

  if [[ "$size1" == "$size2" ]]; then
    echo "Folder $((i+1)) and Folder $((i+2)) have the same size: $size1"
  else
    echo "Folder $((i+1)) and Folder $((i+2)) have different sizes"
  fi


  echo "Directories have the same files:"
  echo "$dir1:"
  echo "$files1"
  echo "$size1"

  echo "$dir2:"
  echo "$files2"
  echo "$size2"

  dir1="${folders[i]}"
  dir2="${folders[i+1]}"
  dirEqualize "$dir1" "$dir2"

#echo "[x] Folders have same name and detected successfully."
#
#
#echo "Review the general results of file syncronisation."
#
#echo "Next step:"
#echo "2. Take necessary actions based on the script's outcome."
#
#echo "3. Cleanup any temporary files or resources, if applicable."
#echo "4. Refer to the documentation for further guidance."
done

